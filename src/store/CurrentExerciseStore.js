// // import Vue from "vue";
// import { HTTP } from "../http-common";
// import EventBus from "../eventBus";

// const CurrentExerciseStore = {
//   data: {
//     id: -1,
//     name: "",
//     videoFileUrl: "",
//     videoFileUpload: null,
//     repetitions: [],
//     // {
//     // startTime: "00:05",
//     // endTime: "00:08"
//     // }

//     internalIdxCounter: 0,
//     ids_marked_to_delete: [],
//   },

//   methods: {
//     addRep(startTime, endTime, apiRepId) {
//       // Vue.set(
//       //   CurrentExerciseStore.data.repetitions,
//       //   CurrentExerciseStore.data.internalIdxCounter,
//       //   {}
//       // );
//       // Vue.set(
//       //   CurrentExerciseStore.data.repetitions[
//       //     CurrentExerciseStore.data.internalIdxCounter
//       //   ],
//       //   "start_time_in_seconds",
//       //   startTime
//       // );
//       // Vue.set(
//       //   CurrentExerciseStore.data.repetitions[
//       //     CurrentExerciseStore.data.internalIdxCounter
//       //   ],
//       //   "end_time_in_seconds",
//       //   endTime
//       // );
//       // Vue.set(
//       //   CurrentExerciseStore.data.repetitions[
//       //     CurrentExerciseStore.data.internalIdxCounter
//       //   ],
//       //   "id",
//       //   apiRepId
//       // );
//       CurrentExerciseStore.data.repetitions.push({
//         internal_index: CurrentExerciseStore.data.internalIdxCounter,
//         start_time_in_seconds: startTime,
//         end_time_in_seconds: endTime,
//         id: apiRepId,
//       });
//       CurrentExerciseStore.data.internalIdxCounter++;
//       CurrentExerciseStore.data.repetitions.sort(
//         (a, b) => a.start_time_in_seconds > b.start_time_in_seconds
//       );
//     },

//     setStartTime(index, startTime) {
//       var target = CurrentExerciseStore.data.repetitions.find(
//         (x) => x.internal_index == index
//       );

//       {
//         target.start_time_in_seconds = startTime;
//         CurrentExerciseStore.data.repetitions.sort(
//           (a, b) => a.start_time_in_seconds > b.start_time_in_seconds
//         );
//       }
//     },

//     setEndTime(index, endTime) {
//       var target = CurrentExerciseStore.data.repetitions.find(
//         (x) => x.internal_index == index
//       );

//       {
//         target.end_time_in_seconds = endTime;
//         CurrentExerciseStore.data.repetitions.sort(
//           (a, b) => a.start_time_in_seconds > b.start_time_in_seconds
//         );
//         console.log("[setEndTime] target: ", JSON.stringify(target));
//       }
//     },

//     delete(index, apiID) {
//       var target = CurrentExerciseStore.data.repetitions.find(
//         (x) => x.internal_index == index
//       );

//       var indexOf = CurrentExerciseStore.data.repetitions.indexOf(target);
//       console.log(
//         "Deleting: " + index,
//         " API ID: " + apiID,
//         "Index of: ",
//         indexOf
//       );

//       if (target != null) {
//         if (target.id != null) {
//           CurrentExerciseStore.data.ids_marked_to_delete.push(target.id);
//         }

//         CurrentExerciseStore.data.repetitions.splice(indexOf, 1);
//       } else {
//         console.log(
//           "Could not find target by index: ",
//           index,
//           " and API id: ",
//           apiID
//         );
//       }
//     },

//     setName(name) {
//       CurrentExerciseStore.data.name = name;
//     },

//     internalUpdateCurrentExercise(targetExerciseObj) {
//       CurrentExerciseStore.data.internalIdxCounter = 0;
//       CurrentExerciseStore.data.id = targetExerciseObj.id;
//       CurrentExerciseStore.data.name = targetExerciseObj.name;
//       CurrentExerciseStore.data.repetitions.length = 0;
//       CurrentExerciseStore.data.repetitions = [];

//       CurrentExerciseStore.data.ids_marked_to_delete.length = 0;
//       CurrentExerciseStore.data.ids_marked_to_delete = [];
//       CurrentExerciseStore.data.videoFileUrl = targetExerciseObj.video_file;
//       CurrentExerciseStore.data.videoFileUpload = null;

//       targetExerciseObj.repetitions.forEach((element) => {
//         this.addRep(
//           element.start_time_in_seconds,
//           element.end_time_in_seconds,
//           element.id
//         );
//       });
//     },

//     updateCurrentExerciseFromAPI(objId) {
//       console.log("updateCurrentExerciseFromAPI");
//       HTTP.get("/exercises/" + objId + "/").then((response) => {
//         console.log(response);
//         CurrentExerciseStore.methods.internalUpdateCurrentExercise(
//           response.data
//         );
//       });
//     },

//     commitCurrentExercise() {
//       // HTTP.patch("exercises/" + CurrentExerciseStore.data.id + "/", {
//       //   name: CurrentExerciseStore.data.name,
//       //   repetitions: CurrentExerciseStore.data.repetitions,
//       //   rep_ids_to_delete: CurrentExerciseStore.data.ids_marked_to_delete,
//       // })
//       //   .then((response) => {
//       //     console.log("[commitCurrentExercise] put, response:");
//       //     console.log(response);

//       //     if (response.status == 200) {
//       //       CurrentExerciseStore.methods.internalUpdateCurrentExercise(
//       //         response.data
//       //       );
//       //       EventBus.$emit("onPushAlert", "Saved!", "success");
//       //     } else {
//       //       console.log("[commitCurrentExercise() response status code: ", response.status)
//       //       // EventBus.$emit("onPushAlert", )
//       //     }
//       //   })
//       //   .catch((errors) => {
//       //     console.log("[commitCurrentExercise] put, response errors::");

//       //     console.log(JSON.stringify(errors.response.data));
//       //     EventBus.$emit("onPushAlert", errors.response.data, "danger");
//       //   });
//     },
//   },
// };

// export default CurrentExerciseStore;
