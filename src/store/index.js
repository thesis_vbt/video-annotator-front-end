import Vue from "vue";
import Vuex from "vuex";

import exercise from "./exercise.module";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        exercise,
    }
});