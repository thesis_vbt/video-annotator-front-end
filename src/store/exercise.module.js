import Vue from "vue";
import Vuex from "vuex";
import { HTTP } from "../http-common";
// import EventBus from "../eventBus";
import { alertDanger, alertSuccess } from "../utils";

Vue.use(Vuex);

export const state = {
    id: -1,
    name: "",
    videoFileUrl: "",
    videoFileUpload: null,
    repetitions: [],
    // {
    // startTime: "00:05",
    // endTime: "00:08"
    // }
    internalIdxCounter: 0,
    ids_marked_to_delete: [],
    pose_servers_config: {},
    attr_values: [],
    attribute_ids_to_delete: []
};

export const getters = {
    exerciseName: state => {
        return state.name;
    },

    videoSource: state => {
        return state.videoFileUrl;
    },

    videoFileUpload: state => {
        return state.videoFileUpload;
    }
};

export const mutations = {

    setName(state, name) {
        state.name = name;
    },

    addRepetition(state, repetition) {
        state.repetitions.push({
            internal_index: state.internalIdxCounter,
            start_timestamp: repetition.startTime,
            end_timestamp: repetition.endTime,
            id: repetition.apiRepId,
            attr_values: []
        });

        state.internalIdxCounter++;
        state.repetitions.sort(
            (a, b) => a.start_timestamp > b.start_timestamp
        );
    },

    // addRepetitions(state, repetitions) {
    //     repetitions.forEach((element) => {
    //         state.repetitions.push({
    //             internal_index: state.internalIdxCounter,
    //             start_timestamp: element.startTime,
    //             end_timestamp: element.endTime,
    //             id: element.apiRepId,
    //         });

    //         state.internalIdxCounter++;
    //     });

    //     state.repetitions.sort(
    //         (a, b) => a.start_timestamp > b.start_timestamp
    //     );
    // },

    setCurrentExercise(state, currentExercise) {
        state.currentExercise = currentExercise;
    },

    setStartTime(state, param_obj) {
        var target = state.repetitions.find(
            (x) => x.internal_index == param_obj.index
        );

        {
            target.start_timestamp = param_obj.startTime;
            state.repetitions.sort(
                (a, b) => a.start_timestamp > b.start_timestamp
            );
        }
    },

    // param_obj should be { index: number, endTime: number}
    setEndTime(state, param_obj) {
        var target = state.repetitions.find(
            (x) => x.internal_index == param_obj.index
        );

        {
            target.end_timestamp = param_obj.endTime;
            state.repetitions.sort(
                (a, b) => a.start_timestamp > b.start_timestamp
            );
            console.log("[setEndTime] target: ", JSON.stringify(target));
        }
    },

    setAttrValue(state, param_obj) {
        var target = state.repetitions.find((x) => x.internal_index == param_obj.repIndex);
        console.log(target);
        console.log(param_obj)
        console.log(target.attr_values[param_obj.attrIndex]);
        target.attr_values[param_obj.attrIndex].value = param_obj.value;
        console.log("Set ", JSON.stringify(target.attr_values));
    },

    // param_obj should have attr_name
    addAttribute(state, param_obj) {
        console.log("hit mutation");
        state.repetitions.forEach((repetition) => {
            var target_attr = repetition.attr_values.find((x) => x.id == param_obj.id);
            if (target_attr == null) {
                // this is is referring to the Attribute entity, so it's a foreign key to this AttributeValue instance
                repetition.attr_values.push({ attribute: param_obj.id, value: 0 });
            }
        });
        // var index = state.attribute_ids_to_delete.indexOf(param_obj.id);
        // if (index != -1) {
        //     state.attribute_ids_to_delete.splice(index, 1);
        // }
        console.log(state.repetitions);
    },

    removeAttribute(state, param_obj) {
        console.log("HIT mutation");
        state.repetitions.forEach((repetition) => {
            var lst = repetition.attr_values.filter(x => x.attribute != param_obj.id);
            repetition.attr_values = lst;
        });

        var index = state.attribute_ids_to_delete.indexOf(param_obj.id);
        if (index == -1) {
            state.attribute_ids_to_delete.push(param_obj.id);
        }
        console.log(state.attribute_ids_to_delete)
    },

    deleteRepetition(state, param_obj) {
        //index, apiID
        var target = state.repetitions.find(
            (x) => x.internal_index == param_obj.index
        );

        var indexOf = state.repetitions.indexOf(target);
        console.log(
            "Deleting: " + param_obj.index,
            " API ID: " + param_obj.apiID,
            "Index of: ",
            indexOf
        );

        if (target != null) {
            if (target.id != null) {
                state.ids_marked_to_delete.push(target.id);
            }

            state.repetitions.splice(indexOf, 1);
        } else {
            console.log(
                "Could not find target by index: ",
                param_obj.index,
                " and API id: ",
                param_obj.apiID
            );
        }
    },

    updateCurrentExercise(state, targetExerciseObj) {
        console.log("Got to updateCurrentExercise. Target obj: ");
        console.log(targetExerciseObj.video_file)
        state.internalIdxCounter = 0;
        state.id = targetExerciseObj.id;
        state.name = targetExerciseObj.name;
        state.repetitions.length = 0;
        state.repetitions = [];

        state.ids_marked_to_delete.length = 0;
        state.ids_marked_to_delete = [];
        state.attribute_ids_to_delete = [];
        state.attribute_ids_to_delete.length = 0;
        state.videoFileUrl = targetExerciseObj.videos[0].video_file;
        state.videoFileUpload = null;

        targetExerciseObj.repetitions.forEach((element) => {
            state.repetitions.push({
                internal_index: state.internalIdxCounter,
                start_timestamp: element.start_timestamp,
                end_timestamp: element.end_timestamp,
                id: element.id,
                attr_values: element.attr_values
            });

            state.internalIdxCounter++;
        });


        state.repetitions.sort(
            (a, b) => a.start_timestamp > b.start_timestamp
        );

        // console.log("After updateCurrentExercise");
        // console.log(state.repetitions);
    },
};

//   

export const actions = {

    addAttribute(context, attr_name) {
        context.commit("addAttribute", attr_name);
    },

    async addRepetition(context, repetition) {
        context.commit("addRepetition", repetition);
    },

    setCurrentExercise(context, currentExercise) { // todo: check if arguments work like this on actions. I think I need to pass an object
        console.log("setCurrentExercise");
        context.commit("setCurrentExercise", currentExercise);
    },

    updateCurrentExerciseFromAPI(context, objId) {
        console.log("updateCurrentExerciseFromAPI");
        HTTP.get("/exercises/" + objId + "/").then((response) => {
            console.log(response);
            context.commit("updateCurrentExercise", response.data)
            // CurrentExerciseStore.methods.internalUpdateCurrentExercise(
            //     response.data
            // );
        });
    },

    commitCurrentExerciseToAPI(context) {
        console.log("commitCurrentExerciseToAPI");
        console.log(context.attribute_ids_to_delete);
        HTTP.patch("exercises/" + context.state.id + "/", {
            name: context.state.name,
            repetitions: context.state.repetitions,
            rep_ids_to_delete: context.state.ids_marked_to_delete,
            attribute_ids_to_delete: context.state.attribute_ids_to_delete
        })
            .then((response) => {
                console.log("[commitCurrentExerciseToAPI] put, response:");
                console.log(response);

                if (response.status == 200) {
                    // CurrentExerciseStore.methods.internalUpdateCurrentExercise(
                    //   response.data
                    // );
                    alertSuccess("Saved data!")
                } else {
                    console.log(
                        "[commitCurrentExercise() response status code: ",
                        response.status
                    );
                    // EventBus.$emit("onPushAlert", )
                }
            })
            .catch((errors) => {
                console.log("[commitCurrentExercise] put, response errors::");

                console.log(JSON.stringify(errors.response.data));
                alertDanger(errors.response.data);

            });
    }
};


export default {
    state,
    getters,
    mutations,
    actions
}