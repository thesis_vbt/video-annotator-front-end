import EventBus from "./eventBus";

export function alertSuccess(message) {
    EventBus.$emit("onPushAlert", message, "success");
}

export function alertDanger(message) {
    EventBus.$emit("onPushAlert", message, "danger");
}
