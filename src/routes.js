import Vue from 'vue'
import VueRouter from 'vue-router'

import ExerciseDetails from './views/ExerciseDetails'
import CreateExercise from './views/CreateExercise'
import ListExercises from './views/ListExercises'
import PoseServerConfiguration from './views/PoseServerConfiguration'
import AttributesConfiguration from './views/AttributesConfiguration'

Vue.use(VueRouter)

export const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '*', redirect: '/exercise/new' },
        { path: '/', redirect: '/exercise/new' },
        { path: '/exercise/new', component: CreateExercise },
        { path: '/exercise/all', component: ListExercises },
        { path: '/exercise/:id', component: ExerciseDetails },
        { path: '/poseserverconfig', component: PoseServerConfiguration },
        { path: '/attributesconfig', component: AttributesConfiguration },
    ]
})